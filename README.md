# dew

Simple package manager for DWScript.

Documentation
-------------

Brain dump notes:

dew droplet namespace: company.package
versions: 1.2.3.4
any revision: 1.2.3.x (x must be last)
any minor: 1.2.x

droplet at version: company.package@1.2.3.4

droplet.json informations:

	{
		"name": "company.package",
		"version": "1.2.3.4",
		"description": "company's super duper package",
		"dependencies": {"other.library": "1.2.x"},
		"author": "John Smith",
		"license": "MIT"
	}

