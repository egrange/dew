BEGIN TRANSACTION;

CREATE TABLE accounts (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   email TEXT,
   dt_creation FLOAT
);
CREATE TABLE droplets (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   name TEXT,
   author INTEGER references accounts(id),
   url TEXT,
   dt_creation FLOAT, 
   description TEXT
);
CREATE TABLE "releases" (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   droplet_id INTEGER references droplets(id),
   version TEXT,
   dt FLOAT,
   description TEXT,
   data BLOB
, sha256 BLOB);
CREATE TABLE dependencies (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   release_id INTEGER NOt NULL references releases(id),
   droplet_id INTEGER NOt NULL references droplets(id),
   version TEXT
);
CREATE TABLE project (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   name TEXT,
   version TEXT,
   release_id INTEGER references releases(id)
);
CREATE TABLE namespaces (
   root TEXT PRIMARY KEY,
   account_id INTEGER references accounts(id)
);
CREATE UNIQUE INDEX accounts_email_idx on accounts(email);
CREATE UNIQUE INDEX droplets_name_idx on droplets(name);
CREATE INDEX release_droplet_idx on releases(droplet_id);
CREATE INDEX dependencies_release_idx on dependencies(release_id);
CREATE INDEX dependencies_droplet_idx on dependencies(droplet_id);
CREATE INDEX project_release_idx on project(release_id);
CREATE INDEX namespaces_account_idx on namespaces(account_id);

COMMIT;
