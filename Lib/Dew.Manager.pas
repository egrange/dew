unit Dew.Manager;

interface

uses
   Dew.DB;

type
   TDewDependency = class
      Droplet : String;
      Version : String;
   end;
   TDewDependencies = array of TDewDependency;

   TDewRelease = class
      Description : String;
      Version : String;
      Dependencies : TDewDependencies;
   end;
   TDewReleases = array of TDewRelease;

   TDewDroplet = class
      Name : String;
      Description : String;
      Author : String;
      URL : String;
      Releases : TDewReleases;

      procedure Install(version : String);
      procedure Remove;

      procedure Submit;
   end;

   TDewProject = class
      RootPath : String;
      Dependencies : TDewDependencies;

      procedure Update;
   end;

implementation


finalization



